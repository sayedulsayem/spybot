#include <AFMotor.h>
#include <Servo.h>              // Add library

#define light_FR  A0    //LED Front Right   pin A0 for Arduino Uno
#define light_FL  A1    //LED Front Left    pin A1 for Arduino Uno
#define light_BR  A2    //LED Back Right    pin A2 for Arduino Uno
#define light_BL  A3    //LED Back Left     pin A3 for Arduino Uno
#define horn_Buzz A4    //Horn Buzzer       pin A4 for Arduino Uno

Servo servo;

int servo_position = 0 ;

AF_DCMotor motorFR(1, MOTOR12_64KHZ);
AF_DCMotor motorFL(2, MOTOR12_64KHZ);
AF_DCMotor motorBR(3, MOTOR34_64KHZ);
AF_DCMotor motorBL(4, MOTOR34_64KHZ);

int command;            //Int to store app command state.
int speedCar = 100;     // 50 - 255.
int speed_Coeff = 4;
boolean lightFront = false;
boolean lightBack = false;
boolean horn = false;

void setup() {
  Serial.begin(9600);
  servo.attach (10);
  motorFR.setSpeed(0);
  motorFL.setSpeed(0);
  motorBR.setSpeed(0);
  motorBL.setSpeed(0); 

  } 

void set_speed(int x){
  motorFR.setSpeed(x);
  motorFL.setSpeed(x);
  motorBR.setSpeed(x);
  motorBL.setSpeed(x);
}

void goAhead(){
  motorFR.run(FORWARD);
  motorFL.run(FORWARD);
  motorBR.run(FORWARD);
  motorBL.run(FORWARD);
  set_speed(speedCar);
  Serial.println("goint to front");
  }

void goBack(){
  motorFR.run(BACKWARD);
  motorFL.run(BACKWARD);
  motorBR.run(BACKWARD);
  motorBL.run(BACKWARD);
  set_speed(speedCar);
  Serial.println("goint to back");
  }

void goRight(){
  motorFR.run(BACKWARD);
  motorFL.run(FORWARD);
  motorBR.run(BACKWARD);
  motorBL.run(FORWARD);
  set_speed(speedCar);
  Serial.println("going to right");
  }

void goLeft(){
  motorFR.run(FORWARD);
  motorFL.run(BACKWARD);
  motorBR.run(FORWARD);
  motorBL.run(BACKWARD);
  set_speed(speedCar);
  Serial.println("going to left");
  }

void servoRotateRight(){
  if(servo_position>=0){
  servo_position=servo_position-1;
  Serial.println(servo_position);
    servo.write(servo_position);
  }
  }
void servoRotateLeft(){
  if(servo_position<=180){
  servo_position=servo_position+1;
  Serial.println(servo_position);
    servo.write(servo_position);
  }
  
  }
  
//void goBackRight(){ 
//
//      digitalWrite(IN_11, LOW);
//      digitalWrite(IN_12, HIGH);
//      analogWrite(ENA_m1, speedCar/speed_Coeff);
//
//
//      digitalWrite(IN_13, HIGH);
//      digitalWrite(IN_14, LOW);
//      analogWrite(ENB_m1, speedCar/speed_Coeff);
//
//
//      digitalWrite(IN_21, HIGH);
//      digitalWrite(IN_22, LOW);
//      analogWrite(ENA_m2, speedCar);
//
//
//      digitalWrite(IN_23, LOW);
//      digitalWrite(IN_24, HIGH);
//      analogWrite(ENB_m2, speedCar);
//
//  }
//
//void goBackLeft(){ 
//
//      digitalWrite(IN_11, LOW);
//      digitalWrite(IN_12, HIGH);
//      analogWrite(ENA_m1, speedCar);
//
//
//      digitalWrite(IN_13, HIGH);
//      digitalWrite(IN_14, LOW);
//      analogWrite(ENB_m1, speedCar);
//
//
//      digitalWrite(IN_21, HIGH);
//      digitalWrite(IN_22, LOW);
//      analogWrite(ENA_m2, speedCar/speed_Coeff);
//
//
//      digitalWrite(IN_23, LOW);
//      digitalWrite(IN_24, HIGH);
//      analogWrite(ENB_m2, speedCar/speed_Coeff);
//
//  }
//
void stopRobot(){
  motorFR.run(RELEASE);
  motorFL.run(RELEASE);
  motorBR.run(RELEASE);
  motorBL.run(RELEASE);
  Serial.println("stop");
  }
  
void loop(){
  
if (Serial.available() > 0) {
  command = Serial.read();
  Serial.println(command);
  stopRobot();      //Initialize with motors stopped.
  
if (lightFront) {digitalWrite(light_FR, HIGH); digitalWrite(light_FL, HIGH);}
if (!lightFront) {digitalWrite(light_FR, LOW); digitalWrite(light_FL, LOW);}
if (lightBack) {digitalWrite(light_BR, HIGH); digitalWrite(light_BL, HIGH);}
if (!lightBack) {digitalWrite(light_BR, LOW); digitalWrite(light_BL, LOW);}
if (horn) {digitalWrite(horn_Buzz, HIGH);}
if (!horn) {digitalWrite(horn_Buzz, LOW);}

switch (command) {
case 'F':goAhead();break;
case 'B':goBack();break;
case 'L':goLeft();break;
case 'R':goRight();break;
case 'I':servoRotateRight();delay(10);break;
case 'G':servoRotateLeft();delay(10);break;
//case 'J':goBackRight();break;
//case 'H':goBackLeft();break;
case '0':speedCar = 100;break;
case '1':speedCar = 115;break;
case '2':speedCar = 130;break;
case '3':speedCar = 145;break;
case '4':speedCar = 160;break;
case '5':speedCar = 175;break;
case '6':speedCar = 190;break;
case '7':speedCar = 205;break;
case '8':speedCar = 220;break;
case '9':speedCar = 235;break;
case 'q':speedCar = 255;break;
case 'W':lightFront = true;break;
case 'w':lightFront = false;break;
case 'U':lightBack = true;break;
case 'u':lightBack = false;break;
case 'V':horn = true;break;
case 'v':horn = false;break;

}
}
}
